Ce dépôt contient deux tutoriels afin de permettre l'alignements de données archéologiques vers la platforme de SIG en ligne [ArkeoGIS](https://arkeogis.org/) à l'aide du logiciel libre R.

[Tutoriel 1](https://lscholtus.gitlab.io/arkeogis-tutoriels/Alignement.html) en français
[Tutoriel 2](https://lscholtus.gitlab.io/arkeogis-tutoriels/Alignment.html) en anglais
